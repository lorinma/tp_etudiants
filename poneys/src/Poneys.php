<?php 
  class Poneys {
      private $count = 0;
      private $maxcount = 15;

      public function getCount() {
        return $this->count;
      }

      public function setFieldSize(int $maxcount){
        $this->maxcount = $maxcount;
      }

      public function setCount(int $count) {
        $this->count = $count;
      }

      public function removePoneyFromField(int $number) {

        //Check for negative number and total negative amount of poneys after operation
        if ($this->count - $number < 0 || $number < 0) { 
          throw new Exception('Negative amount of poneys');
        }

        //Apply operation
        $this->count -= $number;
      }

      public function addPoneyFromField(int $number){

        //Check for negative number
        if ($number < 0) { 
          throw new Exception('Negative amount of poneys');
        }

        //Check if there is room for new poneys
        if ($this->count + $number > $this->maxcount){
          throw new Exception('Too much poneys.');
        }

        //Apply operation
        $this->count += $number;
      }

      public function isSpaceAvailableForPoney(){
        return ($this->count < $this->maxcount);
      }

      public function getNames() {

      }
  }
?>
