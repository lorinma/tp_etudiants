<?php
  require_once 'src/NewClass.php';

  use PHPUnit\Framework\TestCase;

  class NewClassTest extends TestCase {
    public function test_addition(){
        $newclass = new NewClass();
        $this->assertEquals($newclass->addition(1,2),3);
      }
  }
?>