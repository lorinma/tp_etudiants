<?php
  require_once 'src/Poneys.php';

  use PHPUnit\Framework\TestCase;

  class PoneysTest extends TestCase {
    protected $Poneys;

    //At start of each tests
    protected function setUp()
    {
      //Init poneys
      $this->Poneys = new Poneys();
      $this->Poneys->setCount(INITIAL_PONEY_COUNT);
      $this->Poneys->setFieldSize(FIELD_SIZE);
    }

    //At the end of each tests
    protected function tearDown()
    {
      unset($this->Poneys);
    }

    /**
     * @dataProvider removerProvider
     */
    public function test_removePoneyFromField(int $quantity,int $expected) {
      // Action
      $this->Poneys->removePoneyFromField($quantity);
      
      // Assert
      $this->assertEquals($expected, $this->Poneys->getCount());
    }

    /**
     * @expectedException        Exception
     * @expectedExceptionMessage Negative amount of poneys
     */
    public function test_removeNegativePoneyAmount() {
      $this->Poneys->removePoneyFromField(9);
    }

    /**
     * @dataProvider additionProvider
     */
    public function test_addPoneys(int $quantity, int $expected){
      // Action
      $this->Poneys->addPoneyFromField($quantity);

      //Assert
      $this->assertEquals($expected, $this->Poneys->getCount());
    }

    /**
     * @expectedException        Exception
     * @expectedExceptionMessage Negative amount of poneys
     */
    public function test_addNegativePoneysQuantity(){
      // Action
      $this->Poneys->addPoneyFromField(-1);
    }

    /**
     * @expectedException        Exception
     * @expectedExceptionMessage Negative amount of poneys
     */
    public function test_removeNegativePoneysQuantity(){
      $this->Poneys->removePoneyFromField(-1);
    }

    /**
     * @expectedException        Exception
     * @expectedExceptionMessage Too much poneys.
     */
    public function test_appPoneyNoSpaceLeft(){
      $this->Poneys->addPoneyFromField(8);
    }

    public function test_availableSpace(){
      //Assert
      $this->assertTrue($this->Poneys->isSpaceAvailableForPoney());
    }

    public function test_notAvailableSpace(){
      //Fill space
      $this->Poneys->addPoneyFromField(7);

      //Assert
      $this->assertFalse($this->Poneys->isSpaceAvailableForPoney());
    }

    public function test_mockGetNames(){
      // Create a stub for the SomeClass class.
      $this->Poneys = $this->createMock(Poneys::class);
            
      // Configure the stub.
      $this->Poneys
        ->expects($this->exactly(1))
        ->method('getNames')
        ->will($this->returnValue(["John","Marc"]));

      //Assert poneys names
      $this->assertEquals($this->Poneys->getNames(),["John","Marc"]);
    }

     /*
      *   Data Providers
      */
    public function removerProvider(){
      return [
        [3, 8-3],
        [8, 0],
        [2, 8-2]
      ];
    }

    public function additionProvider(){
      return [
        [3, 8+3],
        [6, 8+6],
        [7, 8+7],
        [0, 8]
      ];
    }
  }
 ?>
