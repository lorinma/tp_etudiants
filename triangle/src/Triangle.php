<?php 
  class Triangle {
    public function checkTriangle(){
        //Check argument size
        if (func_num_args() != 3){
            throw new Exception('Invalid Argument numbers');            
        }

        //Get arguments
        $sides = func_get_args(0);

        //Check if arguments are integers
        if(!is_numeric($sides[0]) || !is_numeric($sides[1]) || !is_numeric($sides[2])){
            throw new Exception('Wrong parameters types');
        }

        //Check if arguments are superior than zero
        if ($sides[0] <= 0 || $sides[1] <= 0 || $sides[2] <= 0){
            throw new Exception('Invalid parameters');
        }

        //Check if arguments are not too big
        if ($sides[0] > 9 || $sides[1] > 9 || $sides[2] > 9){
            throw new Exception('Parameter too big');
        }

        //Check for invalid triangles
        if ($sides[0] + $sides[1] <= $sides[2] || 
            $sides[1] + $sides[2] <= $sides[0] || 
            $sides[0] + $sides[2] <= $sides[1] ){
            throw new Exception('Wrong parameters values');
        }

        //Check for equilatera triangles
        if (($sides[0] == $sides[1]) && ($sides[0] == $sides[2])){
            return "equilateral";
        }  

        //Check for isocele triangles
        if (($sides[0] == $sides[1]) ||
             ($sides[0] == $sides[2]) ||
             ($sides[1] == $sides[2])){
            return "isocele";
        } 
        
        //This must be a scalene
        return "scalene";     
    }
  }
?>