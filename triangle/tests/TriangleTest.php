<?php
  require_once 'src/Triangle.php';

  use PHPUnit\Framework\TestCase;

  class TriangleTest extends TestCase {

    //At start of each tests
    protected function setUp()
    {
      //Init poneys
      $this->Triangle = new Triangle();
    }

    /**
     * @expectedException        Exception
     * @expectedExceptionMessage Invalid parameters
     * @dataProvider negativeSetProvider
     */
    public function test_inputSetNegative(int $SideA,int $SideB,int $SideC){
        $this->Triangle->checkTriangle($SideA,$SideB,$SideC);
    }
      public function negativeSetProvider(){
        return [
          [-1, -1, -1],
          [1, -2, -2],
          [-3, 1, -3],
          [-4, -4, 1],
          [1, 1, -5],
          [1, -6, 1],
          [-7, 1, 1]
        ];
      }

    /**
     * @expectedException        Exception
     * @expectedExceptionMessage Invalid parameters
     * @dataProvider zeroSetProvider
     */
    public function test_test_inputSetZero(int $SideA,int $SideB,int $SideC){
        $this->Triangle->checkTriangle($SideA,$SideB,$SideC);
    }
      public function zeroSetProvider(){
        return [
          [0, 0, 0],
          [1, 0, 0],
          [0, 1, 0],
          [0, 0, 1],
          [1, 1, 0],
          [1, 0, 1],
          [0, 1, 1]
        ];
      }

    /**
     * @expectedException        Exception
     * @expectedExceptionMessage Parameter too big
     * @dataProvider greaterThanNineSetProvider
     */
    public function test_inputSetGreaterThanNine(int $SideA,int $SideB,int $SideC){
        $this->Triangle->checkTriangle($SideA,$SideB,$SideC);
    }
      public function greaterThanNineSetProvider(){
        return [
          [10, 11, 12],
          [1, 13, 14],
          [15, 1, 16],
          [17, 18, 1],
          [1, 1, 19],
          [1, 20, 1],
          [21, 1, 1]
        ];
      }

    /**
     * @expectedException        Exception
     * @expectedExceptionMessage Wrong parameters types
     * @dataProvider nonNumericProvider
     */
    public function test_inputSetInputLetter($SideA,$SideB,$SideC){
        $this->Triangle->checkTriangle($SideA,$SideB,$SideC);
    }
    public function nonNumericProvider(){
        return [
          ["A", "B", "C"],
          [1, "C", "C"],
          ["C", 1, "C"],
          ["C", "C", 1],
          [1, 1, "C"],
          [1, "C", 1],
          ["Z", 1, 1]
        ];
      }

    /**
     * @expectedException        Exception
     * @expectedExceptionMessage Wrong parameters types
     * @dataProvider symbolsProvider
     */
    public function test_inputSetCharSymbol($SideA,$SideB,$SideC){
        $this->Triangle->checkTriangle($SideA,$SideB,$SideC);
    }
    public function symbolsProvider(){
        return [
          ["*", ".", "/"],
          [1, "`", "$"],
          ["'", 1, "("],
          ["é", "à", 1],
          [1, 1, "&"],
          [1, "_", 1],
          ["-", 1, 1]
        ];
      }

    /**
     * @expectedException        Exception
     * @expectedExceptionMessage Invalid Argument numbers
     */
    public function test_inputSetTwoElements(){
        $this->Triangle->checkTriangle(1,2);                
    }

    /**
     * @expectedException        Exception
     * @expectedExceptionMessage Invalid Argument numbers
     */
    public function test_inputSetOneElements(){
        $this->Triangle->checkTriangle(1);                
    }

    /**
     * @expectedException        Exception
     * @expectedExceptionMessage Invalid Argument numbers
     */
    public function test_inputSetNoElements(){
        $this->Triangle->checkTriangle();                
    }

    /**
     * @expectedException        Exception
     * @expectedExceptionMessage Invalid Argument numbers
     */
    public function test_inputSetMoreThanThreeElements(){
        $this->Triangle->checkTriangle(1,2,3,4);                
    }

    /**
     * @expectedException        Exception
     * @expectedExceptionMessage Wrong parameters values
     * @dataProvider flatTrianglesProvider
     */
    public function test_inputSetSumOfTwoEqualsThird(int $SideA,int $SideB,int $SideC){
        $this->Triangle->checkTriangle($SideA,$SideB,$SideC);
        
    }
    public function flatTrianglesProvider(){
        return [
          [1,2,3],
          [3,2,1],
          [2,3,1],
          [1,4,5]
        ];
      }

    /**
     * @expectedException        Exception
     * @expectedExceptionMessage Wrong parameters values
     * @dataProvider invalidTrianglesProvider
     */
    public function test_inputSetSumOfTwoLessThanThird(int $SideA,int $SideB,int $SideC){
        $this->Triangle->checkTriangle($SideA,$SideB,$SideC);
    }

    public function invalidTrianglesProvider(){
        return [
          [1,2,4],
          [4,2,1],
          [2,4,1],
          [1,9,2]
        ];
      }

    /**
     * @dataProvider equilateralTrianglesProvider
     */
    public function test_Equilateral(int $SideA,int $SideB,int $SideC){
        $this->assertEquals(
            $this->Triangle->checkTriangle($SideA,$SideB,$SideC),
            "equilateral"
        );              
    }

    public function equilateralTrianglesProvider(){
        return [
          [1,1,1],
          [2,2,2],
          [5,5,5],
          [9,9,9]
        ];
      }

    /**
     * @dataProvider isoceleTrianglesProvider
     */
    public function test_Isocele(int $SideA,int $SideB,int $SideC){
        $this->assertEquals(
            $this->Triangle->checkTriangle($SideA,$SideB,$SideC),
            "isocele"
        );                           
    }

    public function isoceleTrianglesProvider(){
        return [
          [2,3,2],
          [4,5,5],
          [9,9,6]
        ];
      }

    /**
     * @dataProvider scaleneTrianglesProvider
     */
    public function test_Scalene(int $SideA,int $SideB,int $SideC){       
        $this->assertEquals(
            $this->Triangle->checkTriangle($SideA,$SideB,$SideC),
            "scalene"
        );                                 
    }

    public function scaleneTrianglesProvider(){
        return [
          [2,3,4],
          [4,6,5],
          [8,9,6]
        ];
      }
  }
?>